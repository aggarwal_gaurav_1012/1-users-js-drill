// Q4 Find all users with masters Degree.

function getUsersWithMasters(users) {
    const result = [];
    for (const user in users) {
        if (users[user].qualification.includes('Masters')) {
            result.push(user);
        }
    }
    return result;
}

module.exports = getUsersWithMasters;