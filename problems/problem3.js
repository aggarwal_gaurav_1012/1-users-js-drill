// Q3 Sort users based on their seniority level 
//    for desgination - Senior Developer > Developer > Intern
//    for Age - 20 > 10

function seniorityLevel(users) {
    const seniorDevelopers = {};
    const developers = {};
    const interns = {};

    for (const [name, user] of Object.entries(users)) {
        let seniorityLevel;
        
        if (user.desgination.includes("Senior")) {
            seniorityLevel = "Senior";
        } else if (user.desgination.includes("Developer")) {
            seniorityLevel = "Developer";
        } else {
            seniorityLevel = "Intern";
        }

        if (seniorityLevel === "Senior") {
            seniorDevelopers[name] = user;
        } else if (seniorityLevel === "Developer") {
            developers[name] = user;
        } else {
            interns[name] = user;
        }
    }

    const sortObjectByAge = (object) => {
        const sortedKeys = Object.keys(object).sort((a, b) => object[b].age - object[a].age);
        const sortedObject = {};
        for (const key of sortedKeys) {
            sortedObject[key] = object[key];
        }
        return sortedObject;
    };

    const sortedSeniorDevelopers = sortObjectByAge(seniorDevelopers);
    const sortedDevelopers = sortObjectByAge(developers);
    const sortedInterns = sortObjectByAge(interns);

    let obj = Object.fromEntries((Object.entries(sortedSeniorDevelopers)).concat(Object.entries(sortedDevelopers), Object.entries(sortedInterns)));

    return obj;
}

module.exports = seniorityLevel;