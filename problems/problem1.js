// Q1 Find all users who are interested in playing video games.

function playingVideoGames(users) {
    const interestedUsers = [];
    
    for (const username in users) {
        const user = users[username];
        if (user.interests && user.interests[0].includes("Video Games")) {
            interestedUsers.push(username);
        }
    }
    return interestedUsers;
}

module.exports = playingVideoGames;