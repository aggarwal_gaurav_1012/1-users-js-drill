// Q5 Group users based on their Programming language mentioned in their desgination.

function groupUsersByLanguage(users) {
    const groupedUsers = {};

    for (const userName in users) {
        const user = users[userName];
        const desgination = user.desgination;

        let language;
        if (desgination.includes('Golang')) {
            language = 'Golang';
        } else if (desgination.includes('Javascript')) {
            language = 'Javascript';
        } else if (desgination.includes('Python')) {
            language = 'Python';
        } else {
            language = 'Other';
        }

        if (!groupedUsers[language]) {
            groupedUsers[language] = [];
        }
        groupedUsers[language].push(userName);
    }
    return groupedUsers;
}

module.exports = groupUsersByLanguage;