// Q2 Find all users staying in Germany.

function findUsersInGermany(users) {
    
    const usersInGermany = Object.keys(users).filter(userName => users[userName].nationality === 'Germany');
    return usersInGermany;
}

module.exports = findUsersInGermany;