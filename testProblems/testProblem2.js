const findUsersInGermany = require('../problems/problem2.js');

const users = require('../data');

const result = findUsersInGermany(users);

console.log("Users staying in germany:", result);