const seniorityLevel = require('../problems/problem3.js');

const users = require('../data');

const result = seniorityLevel(users);

console.log("Sorting based on their seniority level:", result);