const getUsersWithMasters = require('../problems/problem4.js');

const users = require('../data');

const result = getUsersWithMasters(users);

console.log("Users with master degree:", result);