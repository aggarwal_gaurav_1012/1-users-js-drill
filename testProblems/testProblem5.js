const groupUsersByLanguage = require('../problems/problem5.js');

const users = require('../data');

const result = groupUsersByLanguage(users);

console.log("Users based on their Programming language:", result);